import { Pipe, PipeTransform } from '@angular/core';
import { ResponseCharacterPokemon } from '../models/models-interface';

@Pipe({
  name: 'filterPokemon'
})

export class FilterPipe implements PipeTransform {
 
  transform(allPokemons: ResponseCharacterPokemon[], ...args: unknown[]): ResponseCharacterPokemon[] {
  const filteredPokemon : string = args[0] as string;
  return allPokemons.filter((pokemon: ResponseCharacterPokemon) => {
  const pokemonUpperCase: string = pokemon.name.toUpperCase();
  const filteredPokemonUpperCase: string = filteredPokemon ? filteredPokemon.toUpperCase() : '';
  return pokemonUpperCase.includes(filteredPokemonUpperCase);
  });
  }
 }  