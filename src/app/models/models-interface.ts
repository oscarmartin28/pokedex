export interface ResponseCharacterPokemon {
    abilities: Abilities[];
    base_experience: number;
    id: number;
    moves: Moves[];
    name: string;
    order: number;
    sprites: Sprites;
    types: Types[];
}

export interface Entrenador {
    name: string;
    id: number;
    url: string;
}

export interface Abilities {
    ability: Ability;
}

export interface Ability {
    name: string;
}

export interface Moves {
    move: Move;
}

export interface Move {
    name: string;
}

export interface Sprites {
    front_default: string;
    front_shiny: string;
}

export interface Types {
    type: Type;
}

export interface Type {
    name: string;
}
