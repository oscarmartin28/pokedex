import { ResponseCharacterPokemon } from './../../models/models-interface';
import { PokemonServiceService } from './../../services/pokemonapi/pokemon-service.service';
import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {
  
  public pokemonDetail: ResponseCharacterPokemon;
  private pokemonId: string;

  constructor( private pokemonServiceService: PokemonServiceService, private location: Location, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.getDetail();
  }

  public getDetail(): ResponseCharacterPokemon{
    this.route.paramMap.subscribe(params => {
      this.pokemonId = params.get('id');
    });
    this.pokemonServiceService.getPokemon(Number(this.pokemonId)).subscribe(
      (result) => {
        this.pokemonDetail = result;
      },
      (err) => {
        console.error(err.message);
      }
    );
    return this.pokemonDetail;
  }

  public goBack(): void{
    this.location.back();
  }

}
