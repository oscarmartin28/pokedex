import { ResponseCharacterPokemon } from './../../../models/models-interface';
import { Component, OnInit } from '@angular/core';
import { Input } from '@angular/core';

@Component({
  selector: 'app-pokemon-detail',
  templateUrl: './pokemon-detail.component.html',
  styleUrls: ['./pokemon-detail.component.scss']
})
export class PokemonDetailComponent implements OnInit {

  @Input() pokemonDetail: ResponseCharacterPokemon;

  constructor() { }

  ngOnInit(): void {
  }

}
