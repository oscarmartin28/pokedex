import { ResponseCharacterPokemon } from './../../../models/models-interface';
import { Input } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { PokemonServiceService } from 'src/app/services/pokemonapi/pokemon-service.service';

@Component({
  selector: 'app-pokemon-list',
  templateUrl: './pokemon-list.component.html',
  styleUrls: ['./pokemon-list.component.scss']
})
export class PokemonListComponent implements OnInit {
  @Input() pokemonList: ResponseCharacterPokemon[];
  @Input() pokemonList1: ResponseCharacterPokemon[];
  @Input() pokemonList2: ResponseCharacterPokemon[];
  @Input() pokemonList3: ResponseCharacterPokemon[];
  @Input() pokemonList4: ResponseCharacterPokemon[];
  @Input() pokemonList5: ResponseCharacterPokemon[];
  @Input() pokemonList6: ResponseCharacterPokemon[];
  @Input() pokemonList7: ResponseCharacterPokemon[];
  @Input() pokemonList8: ResponseCharacterPokemon[];
  mostrar: number;
  listado: ResponseCharacterPokemon[] = [];
  public filter: string;
  public pokemonFilteredList: ResponseCharacterPokemon[];

  constructor( private pokemonServiceService: PokemonServiceService ) { 
    this.mostrar = 10;
  }

  public filterPokemon = '';

  ngOnInit(): void {
    console.log(this.mostrar);
    this.addList();
  }

  public mostrar10(): number {
    this.mostrar = 10;
    return this.mostrar;
  }
  public mostrar1(): number {
    this.mostrar = 1;
    return this.mostrar;
  }
  public mostrar2(): number {
    this.mostrar = 2;
    return this.mostrar;
  }
  public mostrar3(): number {
    this.mostrar = 3;
    return this.mostrar;
  }
  public mostrar4(): number {
    this.mostrar = 4;
    return this.mostrar;
  }
  public mostrar5(): number {
    this.mostrar = 5;
    return this.mostrar;
  }
  public mostrar6(): number {
    this.mostrar = 6;
    return this.mostrar;
  }
  public mostrar7(): number {
    this.mostrar = 7;
    return this.mostrar;
  }
  public mostrar8(): number {
    this.mostrar = 8;
    return this.mostrar;
  }

  public onChangeFilter(filter: string): void {
    const newList: ResponseCharacterPokemon[] = this.pokemonFilteredList.filter(element => element.name.toLowerCase().includes(filter.trim().toLowerCase()));
    this.pokemonFilteredList = newList;
    }
  
  public addList(): ResponseCharacterPokemon[]{
      for ( let i = 1; i < 20; i++) {
        this.pokemonServiceService.getPokemon(i).subscribe(
          result => {
            this.listado.push(result);
            this.sortPokemonList(this.listado);
          },
          err => {
            console.error(err.message);
          }
        );
      }
      console.log(this.listado);
      return this.listado;
  }

  private sortPokemonList(array: ResponseCharacterPokemon[]): void{
    array.sort(((a, b) => a.id - b.id));
  }
  
  public onScroll() {
    console.log('scrolled');
    this.addList();
  }

}
