import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { FilterPipe } from './../../pipe/filter.pipe';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PokedexRoutingModule } from './pokedex-routing.module';
import { PokedexComponent } from './pokedex.component';
import { PokemonListComponent } from './pokemon-list/pokemon-list.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [PokedexComponent, PokemonListComponent, FilterPipe],
  imports: [
    CommonModule,
    PokedexRoutingModule,
    FormsModule,
    InfiniteScrollModule
  ]
})
export class PokedexModule { }
