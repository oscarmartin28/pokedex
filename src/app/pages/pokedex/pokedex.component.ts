import { ResponseCharacterPokemon, Types } from './../../models/models-interface';
import { PokemonServiceService } from './../../services/pokemonapi/pokemon-service.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pokedex',
  templateUrl: './pokedex.component.html',
  styleUrls: ['./pokedex.component.scss']
})
export class PokedexComponent implements OnInit {

  public pokemonList: ResponseCharacterPokemon[] = [];
  public pokemonList1: ResponseCharacterPokemon[] = [];
  public pokemonList2: ResponseCharacterPokemon[] = [];
  public pokemonList3: ResponseCharacterPokemon[] = [];
  public pokemonList4: ResponseCharacterPokemon[] = [];
  public pokemonList5: ResponseCharacterPokemon[] = [];
  public pokemonList6: ResponseCharacterPokemon[] = [];
  public pokemonList7: ResponseCharacterPokemon[] = [];
  public pokemonList8: ResponseCharacterPokemon[] = [];

  constructor( private pokemonServiceService: PokemonServiceService ) {
  }

  ngOnInit(): void {
    this.addAllGenerations();
    this.addGenerations1();
    this.addGenerations2();
    this.addGenerations3();
    this.addGenerations4();
    this.addGenerations5();
    this.addGenerations6();
    this.addGenerations7();
    this.addGenerations8();
  }

  addAllGenerations(): ResponseCharacterPokemon[]{
    for ( let i = 1; i < 40; i++) {
      this.pokemonServiceService.getPokemon(i).subscribe(
        result => {
          this.pokemonList.push(result);
          this.sortPokemonList(this.pokemonList);
        },
        (err) => {
          console.error(err.message);
        }
      );
    }
    return this.pokemonList;
  }

  addGenerations1(): ResponseCharacterPokemon[]{
    for ( let i = 1; i < 152; i++) {
      this.pokemonServiceService.getPokemon(i).subscribe(
        result => {
          this.pokemonList1.push(result);
          this.sortPokemonList(this.pokemonList1);
        },
        (err) => {
          console.error(err.message);
        }
      );
    }
    return this.pokemonList1;
  }

  addGenerations2(): ResponseCharacterPokemon[]{
    for ( let i = 152; i < 252; i++) {
      this.pokemonServiceService.getPokemon(i).subscribe(
        result => {
          this.pokemonList2.push(result);
          this.sortPokemonList(this.pokemonList2);
        },
        (err) => {
          console.error(err.message);
        }
      );
    }
    return this.pokemonList2;
  }

  addGenerations3(): ResponseCharacterPokemon[]{
    for ( let i = 252; i < 387; i++) {
      this.pokemonServiceService.getPokemon(i).subscribe(
        result => {
          this.pokemonList3.push(result);
          this.sortPokemonList(this.pokemonList3);
        },
        (err) => {
          console.error(err.message);
        }
      );
    }
    return this.pokemonList3;
  }

  addGenerations4(): ResponseCharacterPokemon[]{
    for ( let i = 387; i < 494; i++) {
      this.pokemonServiceService.getPokemon(i).subscribe(
        result => {
          this.pokemonList4.push(result);
          this.sortPokemonList(this.pokemonList4);
        },
        (err) => {
          console.error(err.message);
        }
      );
    }
    return this.pokemonList4;
  }

  addGenerations5(): ResponseCharacterPokemon[]{
    for ( let i = 494; i < 650; i++) {
      this.pokemonServiceService.getPokemon(i).subscribe(
        result => {
          this.pokemonList5.push(result);
          this.sortPokemonList(this.pokemonList5);
        },
        (err) => {
          console.error(err.message);
        }
      );
    }
    return this.pokemonList5;
  }

  addGenerations6(): ResponseCharacterPokemon[]{
    for ( let i = 650; i < 722; i++) {
      this.pokemonServiceService.getPokemon(i).subscribe(
        result => {
          this.pokemonList6.push(result);
          this.sortPokemonList(this.pokemonList6);
        },
        (err) => {
          console.error(err.message);
        }
      );
    }
    return this.pokemonList6;
  }

  addGenerations7(): ResponseCharacterPokemon[]{
    for ( let i = 722; i < 810; i++) {
      this.pokemonServiceService.getPokemon(i).subscribe(
        result => {
          this.pokemonList7.push(result);
          this.sortPokemonList(this.pokemonList7);
        },
        (err) => {
          console.error(err.message);
        }
      );
    }
    return this.pokemonList7;
  }

  addGenerations8(): ResponseCharacterPokemon[]{
    for ( let i = 810; i < 899; i++) {
      this.pokemonServiceService.getPokemon(i).subscribe(
        result => {
          this.pokemonList8.push(result);
          this.sortPokemonList(this.pokemonList8);
        },
        (err) => {
          console.error(err.message);
        }
      );
    }
    return this.pokemonList8;
  }

  private sortPokemonList(array: ResponseCharacterPokemon[]): void{
    array.sort(((a, b) => a.id - b.id));
  }
}
