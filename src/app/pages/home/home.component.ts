import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
    mostrar: boolean;
  constructor() { 
    this.mostrar = false;
  }

  ngOnInit(): void {
  }

  public bienvenida(): void{
    if ( this.mostrar === false){
        this.mostrar = true;
    }
  }

}
