import { Input } from '@angular/core';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-about-pokedex',
  templateUrl: './about-pokedex.component.html',
  styleUrls: ['./about-pokedex.component.scss']
})
export class AboutPokedexComponent implements OnInit {
  @Input() texto: string;

  constructor() { }

  ngOnInit(): void {
  }

}
