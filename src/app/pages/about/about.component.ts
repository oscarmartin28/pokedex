import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {

    texto: string;

  constructor() { 
    this.texto = 'El objetivo de la aplicación es la creación de una pokédex. Una Pokédex es como una enciclopedia con la que cualquiera puede consultar datos de todos los Pokémon existentes, como las debilidades y resistencias de su tipo frente a otros, los ataques que puede aprender, ya sea por nivel, MTs o crianza, una información general como su tipo o los diveros nombres que tiene en cada país, etc, las habilidades que puede poseer, su línea evolutiva si la tuviera, la descripción que le da cada Pokédex en los diversos juegos de consola, el grupo huevo al que pertenece para crianza, estrategia competitiva o sus stats base. Una información realmente útil tanto para jugadores que solo quieren saber algo del Pokémon como para los que busquen algún set competitivo de algún Pokémon en concreto.'}

  ngOnInit(): void {
  }

}
