import { PokemonServiceService } from './../../services/pokemonapi/pokemon-service.service';
import { ResponseCharacterPokemon, Entrenador } from './../../models/models-interface';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})

export class CreateComponent implements OnInit {
  public pokemonForm: FormGroup | any = null;
  public submitted: boolean = false;
  public showModal: boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private pokemonServiceService: PokemonServiceService
  ) {}

  ngOnInit(): void {
    this.createPokemonForm();
  }

  public createPokemonForm(): void {
    this.pokemonForm = this.formBuilder.group({
      id: ['', [Validators.required, Validators.maxLength(20)]],
      name: ['', [Validators.required, Validators.maxLength(20)]],
      url: ['', [Validators.required, Validators.minLength(5)]],
    });
    console.log(this.pokemonForm);
  }

  public onSubmit(): void {
    this.submitted = true;
    this.showModal = true;
    if (this.pokemonForm.valid) {
      this.postPokemon();
      this.pokemonForm.reset();
      this.submitted = false;
    }
  }

  public postPokemon(): void {
    const entrenador: Entrenador = {
      id: this.pokemonForm.get('id').value,
      name: this.pokemonForm.get('name').value,
      url: this.pokemonForm.get('url').value,
    };
    this.pokemonServiceService.postPokemon(entrenador).subscribe(
      (data: Entrenador) => {
        console.log(data);
      },
      (err) => {
        console.error(err.message);
      }
    );
  }
}
