import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EntrenadoresRoutingModule } from './entrenadores-routing.module';
import { EntrenadoresComponent } from './entrenadores.component';
import { EntrenadoresListComponent } from './entrenadores-list/entrenadores-list.component';


@NgModule({
  declarations: [EntrenadoresComponent, EntrenadoresListComponent],
  imports: [
    CommonModule,
    EntrenadoresRoutingModule
  ]
})
export class EntrenadoresModule { }
