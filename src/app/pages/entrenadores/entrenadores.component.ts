import { PokemonServiceService } from './../../services/pokemonapi/pokemon-service.service';
import { Component, OnInit } from '@angular/core';
import { Entrenador } from 'src/app/models/models-interface';

@Component({
  selector: 'app-entrenadores',
  templateUrl: './entrenadores.component.html',
  styleUrls: ['./entrenadores.component.scss']
})
export class EntrenadoresComponent implements OnInit {

  public entrenadoresList: Entrenador[] = [];

  constructor( private pokemonServiceService: PokemonServiceService) { }

  ngOnInit(): void {
    this.addAllEntrenadores();
  }
 
  addAllEntrenadores(): Entrenador[]{
      this.pokemonServiceService.getEntrenador().subscribe(
        (result: Entrenador[]) => {
          this.entrenadoresList = result;
        },
        (err) => {
          console.error(err.message);
        }
      );
    console.log(this.entrenadoresList);
    return this.entrenadoresList;
  } 

}
