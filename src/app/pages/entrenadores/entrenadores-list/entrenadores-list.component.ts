import { Component, Input, OnInit } from '@angular/core';
import { Entrenador } from 'src/app/models/models-interface';

@Component({
  selector: 'app-entrenadores-list',
  templateUrl: './entrenadores-list.component.html',
  styleUrls: ['./entrenadores-list.component.scss']
})
export class EntrenadoresListComponent implements OnInit {

  @Input() entrenadoresList: Entrenador[];

  constructor() { }

  ngOnInit(): void {
  }

}
