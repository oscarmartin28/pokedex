import { ResponseCharacterPokemon, Entrenador } from './../../models/models-interface';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { URL } from '../../key/url';

@Injectable({
  providedIn: 'root'
})
export class PokemonServiceService {

  private backEntrenadores = URL.backEntrenadores;
  private pokemonUrl = URL.apiKey;
  public pokemonList: ResponseCharacterPokemon[] = [];

  constructor(private http: HttpClient) { }

  public getPokemon(i: number): Observable<ResponseCharacterPokemon> {
    return this.http.get(`${this.pokemonUrl}${i}`).pipe(
      map((response: ResponseCharacterPokemon) => {
        if (!response) {
          throw new Error('Value expected!');
        } else {
          return response;
        }
      }),
      catchError((err) => {
        throw new Error(err.message);
      })
    );
  }

  public getEntrenador(): Observable<Entrenador[]> {
    return this.http.get(this.backEntrenadores).pipe(
      map((response: Entrenador[]) => {
        if (!response) {
          throw new Error('Value expected!');
        } else {
          return response;
        }
      }),
      catchError((err) => {
        throw new Error(err.message);
      })
    );
  }

  public postPokemon(entrenador: Entrenador): Observable<Entrenador> {
    return this.http.post(this.backEntrenadores, entrenador).pipe(
      map((response: Entrenador) => {
        if (!response) {
          throw new Error('Value expected!');
        } else {
          return response;
        }
      }),
      catchError((err) => {
        throw new Error(err.message);
      })
    );
  }
}

